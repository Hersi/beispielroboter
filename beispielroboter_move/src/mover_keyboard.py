#!/usr/bin/env python
import getch
import roslib 
import rospy

from std_msgs.msg      import Float64

#Festlegen der Tastatur Werte
KEY_UP = 65
KEY_DOWN = 66
KEY_RIGHT = 67
KEY_LEFT = 68
USER_QUIT = 100

#Festlegen von maximal Werte
MAX_FORWARD = 11
MAX_LEFT = 10
MIN_FORWARD = -10
MIN_LEFT = -10

#Festlegen fuer aktuell abgefragte taste
keyPress = 0

#Festlegen der joint variablen
jcd1 = 0.0
jcd2 = 0.0
jcd3 = 0.0
jcd4 = 0.0

#Callbackfunktionen fuer alle 4 Joints
def cbJointController1(data):
	global jcd1
	jcd1 = data

def cbJointController2(data):
	global jcd2
	jcd2 = data

def cbJointController3(data):
	global jcd3
	jcd3 = data

def cbJointController4(data):
	global jcd4
	jcd4 = data


#Abonnieren der Topics der einzelnen Joints
sub_control_1 = rospy.Subscriber('/BeispielRoboter/joint1_position_controller/command', Float64, cbJointController1)
sub_control_2 = rospy.Subscriber('/BeispielRoboter/joint2_position_controller/command', Float64, cbJointController2)
sub_control_3 = rospy.Subscriber('/BeispielRoboter/joint3_position_controller/command', Float64, cbJointController3)
sub_control_4 = rospy.Subscriber('/BeispielRoboter/joint4_position_controller/command', Float64, cbJointController4)

#Funktionsaufruf zum Setzen oder lesen des Topics, sowie Logik zum Fahren des Roboters
def gazebo_control():
	#Festlegen von Variablen zum publishen der Topics
	pub_control_1 = rospy.Publisher('/BeispielRoboter/joint1_position_controller/command', Float64)
	pub_control_2 = rospy.Publisher('/BeispielRoboter/joint2_position_controller/command', Float64)
	pub_control_3 = rospy.Publisher('/BeispielRoboter/joint3_position_controller/command', Float64)
	pub_control_4 = rospy.Publisher('/BeispielRoboter/joint4_position_controller/command', Float64)

	#Node muss zuerst initialisiert werden damit Werte veroeffentlicht werden koennen	
	rospy.init_node('beispielroboter_move')

	#Wenn jcd4 noch keinen Wert besitzt dann initialisieren durch callback
	if jcd4 == 0.0:
		pub_control_4.publish(0)
		pub_control_3.publish(0)
		pub_control_2.publish(0)
		pub_control_1.publish(0)
	
	#Auf eingabe warten..
	keyPress = getch.getArrow()
	#Logik fuer das Steuern des Roboters
	if((keyPress == KEY_UP) and (jcd4.data <= MAX_FORWARD and jcd3.data <= MAX_FORWARD and jcd2.data <= MAX_FORWARD and jcd1.data <= MAX_FORWARD)):
		pub_control_4.publish(jcd4.data + 1.0)
		pub_control_3.publish(jcd3.data + 1.0)
		pub_control_2.publish(jcd2.data + 1.0)
		pub_control_1.publish(jcd1.data + 1.0)
	elif((keyPress == KEY_DOWN) and (jcd4.data >= MIN_FORWARD and jcd3.data >= MIN_FORWARD and jcd2.data >= MIN_FORWARD and jcd1.data >= MIN_FORWARD)):
		pub_control_4.publish(jcd4.data - 1.0)
		pub_control_3.publish(jcd3.data - 1.0)
		pub_control_2.publish(jcd2.data - 1.0)
		pub_control_1.publish(jcd1.data - 1.0)
	elif((keyPress == KEY_LEFT)):
		pub_control_4.publish(jcd4.data - 1.0)
		pub_control_3.publish(jcd3.data + 1.0)
		pub_control_2.publish(jcd2.data - 1.0)
		pub_control_1.publish(jcd1.data + 1.0)
	elif((keyPress == KEY_RIGHT)):
		pub_control_4.publish(jcd4.data + 1.0)
		pub_control_3.publish(jcd3.data - 1.0)
		pub_control_2.publish(jcd2.data + 1.0)
		pub_control_1.publish(jcd1.data - 1.0)

	#Ausgabe der einzelnen Topicwerten im Terminal
	rospy.loginfo("Wert lenken 1 %f 2 %f 3 %f 4 %f"%(jcd1.data,jcd2.data,jcd3.data,jcd4.data))
	
	return keyPress


while(keyPress != USER_QUIT):
	keyPress = gazebo_control()

exit()
