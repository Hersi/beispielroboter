#!/usr/bin/env python
import getch
import roslib #roslib.load_manifest('BeispielRoboter_move')
import rospy

from tf.transformations import euler_from_quaternion

from geometry_msgs.msg import Twist
from geometry_msgs.msg import Pose
from nav_msgs.msg      import Odometry
from sensor_msgs.msg   import LaserScan
from std_msgs.msg      import Float64


#Globale Variablen Initialisierungen
laserScan = LaserScan()
pose      = Pose()

#Callback fuer die Lasertopicwerte
def cbLaser(laserData):
    global laserScan
    laserScan = laserData

#Callback fuer die Odometrywerte
def cbOdom(odom):
    global pose
    pose = odom.pose.pose

#Funktion fuer den logischen Ablauf fuer das autonome Fahren eines Parcours mit Skidsteer Plugin...
def robot():
    
    #Initialisierung von node und publisher 
    rospy.init_node('beispielroboter_move', anonymous=True)
    velPub = rospy.Publisher('/cmd_vel', Twist)

    r = rospy.Rate(10)
    
    twist = Twist() 
    
    
    while not rospy.is_shutdown(): 
        #derzeitige Position ermitteln
        x = pose.position.x
        y = pose.position.y
        
        
        if len(laserScan.ranges) > 135:
            str = "Rangemin/max %f %f %f %f %f %f"%(laserScan.range_min, laserScan.range_max, len(laserScan.ranges), laserScan.ranges[45], laserScan.ranges[90], laserScan.ranges[135])
            rospy.loginfo(str)
###########################
	    if laserScan.ranges[45] < 0.8:
			rospy.loginfo("LINKS:")
			rospy.loginfo("links: %f geradeaus: %f rechts: %f"%(laserScan.ranges[25],laserScan.ranges[90], laserScan.ranges[155] ))
			if twist.angular.z > -5:
				twist.angular.z = twist.angular.z - 1
			if twist.linear.x > 0.2:
				twist.linear.x = twist.linear.x - 0.1
			rospy.sleep(0.15)

##########################
	    elif laserScan.ranges[135] < 0.8:
			rospy.loginfo("RECHTS:")
			rospy.loginfo("links: %f geradeaus: %f rechts: %f"%(laserScan.ranges[25],laserScan.ranges[90], laserScan.ranges[155] ))
			if twist.angular.z < 5:
				twist.angular.z = twist.angular.z + 1
			if twist.linear.x > 0.2:
				twist.linear.x = twist.linear.x - 0.1
			rospy.sleep(0.15)
#########################
 	    elif laserScan.ranges[90] < 1.2:
			rospy.loginfo("WARNUNG!!!!:")
			rospy.loginfo("links: %f geradeaus: %f rechts: %f"%(laserScan.ranges[25],laserScan.ranges[90], laserScan.ranges[155] ))
			if laserScan.ranges[45] < laserScan.ranges[135]:
				twist.angular.z = -2.0
				twist.linear.x = 0.0
			else:
				twist.angular.z = 2.0
				twist.linear.x = 0
	    else:
			rospy.loginfo("GERADEAUS:")
			rospy.loginfo("links: %f geradeaus: %f rechts: %f"%(laserScan.ranges[25],laserScan.ranges[90], laserScan.ranges[155] ))
			twist.angular.z = 0.0
			if twist.linear.x < 0.5:
				twist.linear.x = twist.linear.x + 0.1
			rospy.sleep(0.15)
	log = "speed: ", twist.linear.x, "lenkung: ", twist.angular.z
	rospy.loginfo(log)
        velPub.publish(twist)

 	r.sleep()
 
#Abonnieren von Odometrie-Daten
odomSub = rospy.Subscriber('/odom', Odometry, cbOdom) 

#Abonnieren von LaserDaten
laserSub = rospy.Subscriber('/laserscan', LaserScan, cbLaser) 
robot()		
